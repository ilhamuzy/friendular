import { Component, OnInit } from '@angular/core';

import { Post } from 'src/app/core/post';
import { PostService } from 'src/app/core/post.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
})
export class PostsComponent implements OnInit {
  posts: Post[] = [];

  constructor(private postService: PostService) {}

  ngOnInit(): void {
    this.getPosts();
  }

  getPosts(): void {
    this.postService.getPosts().subscribe((posts) => (this.posts = posts));
  }

  add(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.postService.addPost({ name } as Post).subscribe((post) => {
      this.posts.unshift(post);
    });
  }

  //method DELETE POST
  delete(post : Post): void {
    this.posts = this.posts.filter(p => p !== post);
    this.postService.deletePost(post.id).subscribe();
  }
}
