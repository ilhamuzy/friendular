import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Post } from './post';
@Injectable({
  providedIn: 'root',
})
export class PostService {
  private postUrl: string = 'api/posts';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private http: HttpClient) {}

  /** Get All Posts */
  getPosts(): Observable<Post[]> {
    console.log('coba');
    return this.http.get<Post[]>(this.postUrl).pipe(
      tap((_) => this.log('fetched Posts')),
      catchError(this.handleError<Post[]>('getPosts', []))
    );
  }

  /** POST: add a new hero to the server */
  addPost(post: Post): Observable<Post> {
    return this.http.post<Post>(this.postUrl, post, this.httpOptions).pipe(
      tap((newPost: Post) => this.log(`added post w/ id=${newPost.id}`)),
      catchError(this.handleError<Post>('addPost'))
    );
  }

  /** DELETE: delete the post from the server */
  deletePost(id: number): Observable<Post> {
    const url = `${this.postUrl}/${id}`;

    return this.http.delete<Post>(url, this.httpOptions).pipe(
      tap((_) => this.log(`deleted post id=${id}`)),
      catchError(this.handleError<Post>('deletePost'))
    );
  }

  /** Print message to console */
  private log(message: string) {
    console.log(message);
  }

  /** Handle Error method */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
